<?php
    class Controllers{
        /**
     * Création de la session utilisateur
     * $ doit etre a la valeur 1 pour fonctionner
     * 
     * @param bolean
     * @return true or false
     */
        static function createUserSession($auth) {
            // Var par defaut
                $result = FALSE;
            // Si datas n'est pas vide
            if ($auth=="ok") {
            // Ajout des informations en sessions
                $_SESSION['idClient'] = 1;
                $_SESSION['statutClient'] = "non connecté";
                $_SESSION['statutAdmin'] = "non";
                $_SESSION['nom'] = "Dost";
                $_SESSION['prenom'] = "Raphaël";
                $_SESSION['email'] = "raphaeldost@gmail.com";
                $_SESSION['dejaConnecte'] = "non";
                $_SESSION['tutorielSuivi'] = "non";
                $_SESSION['idAuth'] = VAR_SECURE_AUTH;
                $result = TRUE; 
            }
        return $result;
        }

/**
 * Supprime la session utilisateur
 * La liste des datas a supprimer de la session doivent correspondrent
 * à la liste des datas créé en session du controller : creationSessionUtilisateur
 * 
 * @param none
 * @return none
 */
        static function destroyUserSession() {
    // Var par defaut
            $result = FALSE;
    // Vidage des variables
            unset($_SESSION['idClient']);
            unset($_SESSION['statutClient']);
            unset($_SESSION['statutAdmin']);
            unset($_SESSION['nom']);
            unset($_SESSION['prenom']);
            unset($_SESSION['email']);
            unset($_SESSION['dejaConnecte']);
            unset($_SESSION['tutorielSuivi']);
            unset($_SESSION['idAuth']);
    // On vérifie que la session est bien vidée
            if (!isset($_SESSION['idClient'])) {
                $result = TRUE;
            }
        return $result;
        }

}


// Vérification des variables
/*    var_dump($resultUserSession);
var_dump($resultDestroyUserSession);
var_dump($_SESSION); */


// Suppression de la session
//    $resultUserSession = TestConnexion::destroyUserSession();
//    var_dump($resultUserSession);
//    var_dump($_SESSION);
    