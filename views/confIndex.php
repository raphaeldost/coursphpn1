<?php
    //lancement de la session
    session_start();
    //Encodage de la page
    header ("Content-Type: text/html; charset=utf-8");
    //Configuration générale
    require_once("../conf/generalConf.php");

    // Autoloader global
    require_once (PATH_MACHINE."autoLoader/AutoLoad.php");