<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Test de titre de page du cours PhpN</title>
    <meta name="description" content="Test de description">
    <meta name="keywords" content="planning, formation, tache, todolist, gestion de temps, gestion de contact, addiction">
    <!-- Indexer et suivre -->
    <meta name="robots" content="index,follow">
    <!-- Pour dimensionner la page par defaut -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Police du site -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Questrial&display=swap" rel="stylesheet"> 

    <!-- Bootstrap 5.2 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <!-- Surcharge des Css -->
    <link href="<?php echo HTTP_PATH_HOST_PRINCIPAL ?>component/css/cssSurchargeGlobale.css" crossorigin="anonymous" rel="stylesheet" />

</head>