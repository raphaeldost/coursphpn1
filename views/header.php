<?php
    /* TODO
    if lang détecté est différente de fr
    maVar = langue détecté
    sinon = fr */
?>
<!DOCTYPE html>
<html lang="fr">
<?php
// Inclusion du header seul
    require_once ("headerAlone.php");
?>
<body>

<!-- Entete avec logo et Connexion/Deconnexion -->
<div class="container col-12">
    <div class="container col-12">
        <div class="row justify-content-between">
            <div class="logo">
                <a href=""><img class="img-fluid w-25" src="<?php echo HTTP_PATH_HOST_PRINCIPAL ?>component/img/logo.jpg" alt="Logo igestPro" title=""></a>
                <a class="familyLogoText orange" href="" title=""> Test </a>
                <a class="familyLogoText gris" href="" title=""> Test 2 </a>
            </div>
            <!-- Langue du site -->
                <div class="changeLangue">
                    <?php
                    // Parcours des langues issue de la db pour afficher les boutons selon l'état
                    /*    foreach ($listeLangue as $langue) {
                        // On affiche la langue sélectionnée en couleur spéciale
                            if ($_SESSION["langueEnCours"]==$langue->codeIsoLangue) {
                                $btnSelected = "btnLangueSelected";
                            } else {
                                $btnSelected = "";
                            }
                        // On disable les langues qui ne sont pas en activité
                            if ($langue->enActivite=="non") {
                                $btnDisabled = "disabled='disabled'";
                            } else {
                                $btnDisabled = "";
                            }
                            echo "<button type='button' class='btn btn-dark btn-sm ".$btnSelected."' onClick=\"window.location.href='".HTTP_PATH_HOST_PRINCIPAL.HTTP_PATH_VIEWS."index.php?lg=".$langue->codeIsoLangue."&origin=".$headerService."'\" title='".$langue->nomLangue."' $btnDisabled>".$langue->affichetextBouton." <i class='fas fa-redo-alt smallSize'></i></button>&nbsp;";
                        }*/
                    ?>
                </div>
        
        </div>
    </div>

<!-- Barre de navigation pour le menu header -->
    <nav class="navbar navbar-dark bg-dark mt-3 mb-1">
        <a class="navbar-brand navTextMenu" data-toggle="collapse" data-target="#navbarSupported" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" href="#"></a>
        <?php
        //$_SESSION['statutClient'] = 1;
            
            // Affiche qui est connecté
            if (@$_SESSION["statutClient"]!=0) {
        ?>
            <span class="navbar-brand connecteSous"><a href="#" title="<?php echo @$_SESSION["email"] ?>"> <?php echo @$_SESSION["prenom"]." ".@$_SESSION["nom"] ?></a></span>
        <?php
            } else {
        ?>
        <form method="post">
            <button class="connectionBtn" type="submit">Connnexion</button>
        <span class="navbar-brand"> </span>
        <?php
            }
            if(isset($_POST['connectionBtn'])) 
                $_SESSION['statutClient'] = 1
            
        ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupported" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupported">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active"><a class="navbar-brand nav-link active" href="">Texte blabla</a></li>
                    <?php
                    // Affichage de la liste des services actifs
                        if ($listeServiceValideParIdClient!=FALSE) {
                            echo "<li class='nav-item sousTitreNavItem'>".$txtLangue->txtServiceActif."</li>";
                            foreach ($listeServiceValideParIdClient as $serviceId => $serviceName) {
                                echo "<li class='nav-item'><a class='listeItemNav' href='".HTTP_PATH_HOST_PRINCIPAL.HTTP_PATH_SERVICES.$serviceId."/".HTTP_PATH_VIEWS."' ><i class='fas fa-chevron-right sm'></i> $serviceName</a></li>";
                            }
                        }
                    ?>
                <li class="nav-item"><a class="navbar-brand" href="" data-toggle="modal" data-target="#exampleModal">Tarif</a></li>
                <li class="nav-item"><a class="navbar-brand" href="" data-toggle="modal" data-target="#votreAvis">Votre avis</a></li>
                <li class="nav-item"><a class="navbar-brand" href="" data-toggle="modal" data-target="#votreAvis">Tada</a></li>
                <?php
                //  Vérifie si on est connecté
                    if (isset($_SESSION["idClient"])){
                ?>
                    <li class="nav-item"><a class="navbar-brand" href="index.php?v=profil" ?> Mon profil</a></li>
                <?php
                
                    } else {
                ?>
                    <li class="nav-item"><a class="navbar-brand" href="index.php?v=auth">Bonjour</a></li>
                <?php
                    }
                ?>
                <!--Formulaire pour le moteur de recherche -->
                <!--<li class="nav-item">
                    <form action="index.php"  class="form-inline" method="post">
                        <input class="form-control mr-sm-2" name="recherche" type="search" placeholder="Recherche..." aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                    </form>-->
                </li>
            </ul>
        </div>
    </nav>
